
/**
* @file music.h
* @brief header file for music.h
* @author Anh To
* @date 12/7/18
*/


#ifndef MUSIC_H
#define MUSIC_H

#define RICK_ROLL			0
#define IMERPIAL_MARCH 		1
#define MARIO_UNDERWORLD	3
#define MARIO_UNDERWATER	7
#define BEEP                8

//void init_music();

void load_songs( );

#endif /* MUSIC_H_ */
