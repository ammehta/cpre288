#include "cliffandbumps.h"


int checkCliff(oi_t *sensor)
{
    oi_update(sensor);

    /**
     *  0 = no cliff
     *  1 = Cliff Left
     *  2 = Cliff Right
     *  3 = Cliff Front Left
     *  4 = Cliff Front Right
     */
    int cliff = 0;

    if(sensor-> cliffLeft)
    {
        cliff = 1;
    }
    else if(sensor -> cliffRight)
    {
        cliff = 2;
    }
    else if(sensor -> cliffFrontLeft)
    {
        cliff = 3;
    }
    else if (sensor -> cliffFrontRight)
    {
        cliff = 4;
    }

    else
    {
        cliff = 0;
    }

    return cliff;

}

int checkBumper(oi_t *sensor)
{
    /**
     * Bumper States:
     * wrong this is lights on the bumper
     * bumper = 0, no bumps
     * bumper = 1, right bumper
     * bumper = 2, front right bumper
     * bumper = 3, center right bumper
     * bumper = 4, center left bumper
     * bumper = 5, front left bumper
     * bumper = 6, left bumper
     */
    int bumper = 0;

    oi_update(sensor);
    if (sensor-> lightBumperLeft)
    {
        bumper = 6;
    }
    else if (sensor-> lightBumperFrontLeft)
    {
        bumper = 5;
    }
    else if (sensor-> lightBumperCenterLeft)
        {
            bumper = 4;
        }
    else if (sensor-> lightBumperCenterRight)
    {
        bumper = 3;
    }
    else if (sensor-> lightBumperFrontRight)
    {
        bumper = 2;
    }
    else if (sensor-> lightBumperRight)
    {
        bumper = 1;
    }
    else
    {
        bumper = 0;
    }

    return bumper;
}




