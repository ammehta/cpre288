#ifndef MOVEMENT_H
#define MOVEMENT_H
/**
	@file movement.h
	@brief the header file for our movement functions 

	@author Darron Anderson
	@date 12/7/18
*/
#include "button.h"
#include "Timer.h"
#include "lcd.h"
#include "open_interface.h"
#include "uart.h"  // Functions for communiticate between CyBot and Putty (via UART)
                         // PuTTy: Buad=115200, 8 data bits, No Flow Control, No Party,  COM1
#include "servo.h"
#include "ultrasonic.h"
#include "cyBot_uart.h"
#include "sensor.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/**
 *this function is how we control the robot
 *	@author Darron Anderson
 *	@param Sensor_data the data from the robot
 *	@date 12/7/18
 */
void manuel_control(oi_t *sensor_data);
/**
 * this function allows us to test small things quickly 
 *	@author Darron Anderson
 *	@param Sensor_data the data from the robot
 *	@date 12/7/18
 */
void test_things(oi_t *sensor_data);

/**
 *  this function is how we turn the robot 
 *	@author Darron Anderson
 *	@param sensor_data the data from the robot
 *	@param degrees the number of degrees to turn from -179 to 180
 *	@date 12/7/18
 */
void turn_angle(int angle, oi_t *sensor_data );
/**
 *	this function is how move forward safely in autonomous mode
 *	@author Darron Anderson
 *	@param sensor_data the data from the robot
 *	@param tar distance to move forward
 *	@date 12/7/18
 */
void safe_move_forward(oi_t *sensor_data,int tar);
/**
 *	this function is how we stop the robot
 *	@author Darron Anderson
 *	@date 12/7/18
 */
void stop();
/**
 *	this function is how we check for a wheel drop
 *	@author Darron Anderson
 *	@param sensor_data the data from the robot
 *	@date 12/7/18
 */
int drop_check(oi_t *sensor_data);
/**
 *	this function is how we react to drops in autonomous mode
 *	@author Darron Anderson
 *	@param sensor_data the data from the robot
 *	@param direction which wheel dropped
 *	@date 12/7/18
 */
void drop_reaction(oi_t *sensor_data, int direction);
/**
 *	this function is how check for the cliff sensor
 *	@author Darron Anderson
 *	@param sensor_data the data from the robot
 *	@date 12/7/18
 */
int cliff_check(oi_t *sensor_data);
/**
 *	this function is how we react to cliffs in autonomous mode
 *	@author Darron Anderson
 *	@param sensor_data the data from the robot
 *	@param res the result of cliff check
 *	@date 12/7/18
 */
void cliff_reaction(oi_t *sensor_data, int res);
/**
 *	this function is how we check for bumps
 *	@author Darron Anderson
 *	@param sensor_data the data from the robot
 *	@date 12/7/18
 */
int bump_check(oi_t *sensor_data);
/**
 *	this function is how we react to bumps in autonomous mode
 *	@author Darron Anderson
 *	@param sensor_data the data from the robot
 *	@param direction which wheel dropped
 *	@date 12/7/18
 */
void bumper_reaction(oi_t *sensor_data, int direction);
/**
 *	this function is how we check for edges
 *	@author Darron Anderson
 *	@param sensor_data the data from the robot
 *	@date 12/7/18
 */
int edge_check(oi_t *sensor_data);
/**
 *	this function is how we react to edges in autonomous mode
 *	@author Darron Anderson
 *	@param sensor_data the data from the robot
 *	@param res which sensor is detecting an edge
 *	@date 12/7/18
 */
void edge_reaction(oi_t *sensor_data, int res);

//int found_goal=0;
/**
 * a data type for our scanning functions that stores information about the objects we detect
 *	@author Darron Anderson
 *	@date 12/7/18
 */
typedef struct thing {
    char width;
    char start_angle;
    char distance;
    char end_angle;
    char dist_1;
    char dist_2;
} thing_t;
#endif
