/**
* @file Main.c
* @brief the implementation of our main
* @author Aashray Mehta
* @date 12/7/18
*/


#include "button.h"
#include "Timer.h"
#include "lcd.h"
#include "open_interface.h"
#include "uart.h"  // Functions for communiticate between CyBot and Putty (via UART)
                         // PuTTy: Buad=115200, 8 data bits, No Flow Control, No Party,  
#include "servo.h"
#include "ultrasonic.h"
#include "cyBot_uart.h"
#include "movement.h"
#include "sensor.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "music.h"

//void scan();
//void find_start(oi_t* s);//forward declaration

/**
*	this is the main method it inititiats the servo, the IR, the sonar and the lcd
*	there is also a calibrate method call to calibrate the servo between switching
*	bots  
* 	@author Aashray Mehta
*	@date 12/7/18
*/

void main() {

    oi_t *sensor_data = oi_alloc();
                       oi_init(sensor_data);

                       oi_setWheels(0,0);//stop
                        
           servo_init();
           ultrasonic_init();
           uart_init();
           lcd_init();
        //   calibrate(); // calibrate allows for automatic calibration of the servo
           while(1)
           {
               manuel_control(sensor_data);
           }



}
