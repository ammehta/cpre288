#ifndef SENSOR_H
#define SENSOR_H
#include "button.h"
#include "Timer.h"
#include "lcd.h"
#include "open_interface.h"
#include "uart.h"  // Functions for communiticate between CyBot and Putty (via UART)
                         // PuTTy: Buad=115200, 8 data bits, No Flow Control, No Party,  COM1
#include "servo.h"
#include "ultrasonic.h"
#include "cyBot_uart.h"
//#include "movement.h"
//#include <String.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>



int bump_check(oi_t *sensor_data);
int cliff_check(oi_t *sensor_data);
int drop_check(oi_t * sensor_data);
int edge_check(oi_t *sensor_data);
//may need spcial case for no bump shit
int is_blocked(oi_t *s);
//thing_t *goal_scan(oi_t* s);
int short_scan(int min,int max);
void scan();
#endif
