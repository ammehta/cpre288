/**
 * @file movement.c
 * @brief the implementation of our movement functions
 * 	@author Darron Anderson
	@date 12/7/18
 */
#include "movement.h"
#include "sensor.h"
#include "music.h"
/**
 * this function allows us to test small things quickly 
 *	@author Darron Anderson
 *	@param Sensor_data the data from the robot
 *	@date 12/7/18
 */
void test_things(oi_t* sensor_data)
{
    int i = cliff_check(sensor_data);
    if(!i) {
        lcd_printf("none");
        return;
    }
    if(i&1)  {
        lcd_printf("left");
        return;
    }
    if(i&2) {lcd_printf("front left"); return;}
    if(i&4)  {lcd_printf("front right"); return ;}
    if(i&8)  {lcd_printf("right"); return;}

}
/*
forward referances for several functions we define later
*/
void move_forward(int power);
void turn_to_angle(int angle,int power, oi_t *sensor_data );
int quick_move_forward(oi_t* s,int tar,int power);
void update_status(oi_t* s);

/**
 * this function allows us to test small things quickly 
 *	@author Darron Anderson
 *	@param Sensor_data the data from the robot
 *	@date 12/7/18
 */
void manuel_control(oi_t *sensor_data)
{
//    void load_songs();
    int byte =uart_receive();
                         uart_sendChar(byte);//send the byte
                         uart_sendChar('\r');//send the byte

                         if((char)byte=='w')//based on input change what its doing
                         {
                             if(cliff_check(sensor_data)||drop_check(sensor_data)||edge_check(sensor_data)||bump_check(sensor_data)||is_blocked(sensor_data)){
                                 update_status(sensor_data);
                                         return ;
                                     }

                             quick_move_forward(sensor_data,20,100); // forward
                             stop();

                         }
                         if((char)byte=='e')//based on input change what its doing
                                                 {
                                                     if(cliff_check(sensor_data)||drop_check(sensor_data)||edge_check(sensor_data)||bump_check(sensor_data)||is_blocked(sensor_data)){
                                                         update_status(sensor_data);
                                                                 return ;
                                                             }

                                                     quick_move_forward(sensor_data,10,100); // forward
                                                     stop();

                                                 }
                         if((char)byte=='s')
                                    {
                             oi_play_song(BEEP);
                                        move_back(sensor_data,5,100); //back
                                        stop();
                                    }
                         if((char)byte=='a')
                                    {
                                 turn_angle(10,sensor_data);
                                stop();

                                    }
                         if((char)byte=='d')
                         {
                             turn_angle(-10,sensor_data);
                             stop();
                         }
                         if((char)byte=='x')
                                     {
                                         oi_setWheels(0,0);//stop
                                     }
                         if((char)byte == 'j')
                         {
                             scan();
                         }
                         lcd_printf("%c\n",(char)byte);


                         if((char)byte == '7')
                         {
//                             turn_to_angle(45,100,sensor_data);
                             turn_angle(-45,sensor_data);
                         }
                         if((char)byte == '9')
                         {
//                             turn_to_angle(-45,100,sensor_data);
                             turn_angle(45,sensor_data);
                         }
                         if((char)byte == '3')
                         {
//                             turn_to_angle(-135,100,sensor_data);
                             turn_angle(135,sensor_data);
                         }
                         if((char)byte == '1')
                         {
//                             turn_to_angle(135,100,sensor_data);
                             turn_angle(-135,sensor_data);
                         }
                         if((char)byte == '2')
                         {
//                             turn_to_angle(180,100,sensor_data);
                             turn_angle(180,sensor_data);
                         }
                         if((char)byte == 'o')
                         {
                             oi_play_song(IMERPIAL_MARCH);
                         }
                         update_status(sensor_data);
}
/**
 * this function updates putty with important information about the cybot
 *	@author Darron Anderson
 *	@param Sensor_data the data from the robot
 *	@date 12/7/18
 */
void update_status(oi_t* sensor_data)
{
    char status[500];
    sprintf(status, "\n\rBump sensor_data( Left: %d   Right: %d)\n\rCliff sensor_datas(Left: %d   Front left: %d   Front right: %d   Right: %d)"
            "\n\rCliff sensor_data Signals(Left: %d   Left Front: %d   Right Front: %d   Right: %d\n\r", sensor_data->bumpLeft, sensor_data->bumpRight,
            sensor_data->cliffLeft, sensor_data->cliffFrontLeft, sensor_data->cliffFrontRight, sensor_data->cliffRight, sensor_data->cliffLeftSignal,
            sensor_data->cliffFrontLeftSignal, sensor_data->cliffFrontRightSignal, sensor_data->cliffRightSignal);

    uart_sendStr(status);

//    sprintf(msg,"bump %d, cliff %d, drop %d,edge %d blocked %d\n\r",bump_check(s),cliff_check(s),drop_check(s),edge_check(s),is_blocked(s));
        sprintf(status,"ping %d ir %d\n\r",ping_read(),get_dist());
    uart_sendStr(status);

}
/**
 * this function is a slightly different move forward then safe_move_forward as it stops us 
 *	instead of reacting for manuel control mode
 *	@author Darron Anderson
 *	@param s the data from the robot
 *	@param tar the target distance 
 *	@param power the power to move at 0-500
 *	@date 12/7/18
 */
int quick_move_forward(oi_t* s,int tar,int power)
{
    int dist=0;
    move_forward(power);
    while(dist<tar)
    {
        oi_update(s);
        dist+=s->distance;
        char msg[100];
            sprintf(msg,"dist %d",dist);
            lcd_printf(msg);
        if(cliff_check(s)){
            return 1;
        }
        if(drop_check(s)){
            return 2;
        }
        if(edge_check(s)){
            return 3;
        }
        if(bump_check(s)){
            return 4;
        }
    }
    return 0;
}
/**
 *  this function is how we turn the robot clockwise
 *  if this were c++ this would be private
 *	@author Darron Anderson
 *	@param sensor_data the data from the robot
 *	@param degrees the number of degrees to turn from 0 to 180
 *	@date 12/7/18
 */
void turn_clockwise(oi_t *sensor, int degrees){

        int curr_angle = 0;
        oi_setWheels(-100, 100);    // turns the roomba slowly

        while (degrees+curr_angle>0) {  // Updates current angle to keep track of the degrees turned
             oi_update(sensor);
             curr_angle += sensor->angle;
        }

        oi_setWheels(0, 0);     // Once desired degrees is reached the wheels stop
        timer_waitMillis(300);
}

/**
 *  this function is how we turn the robot counter clockwise
 *  if this were c++ this would be private
 *	@author Darron Anderson
 *	@param sensor_data the data from the robot
 *	@param degrees the number of degrees to turn from -179 to 0
 *	@date 12/7/18
 */
void turn_counterclockwise(oi_t *sensor, int degrees){

    degrees=-degrees;   // Converts the degrees to negative (More user friendly)

    int curr_angle = 0;
    oi_setWheels(100, -100);    // turns the roomba slowly

    while (degrees+curr_angle<0) {      // Updates current angle to keep track of the degrees turned
         oi_update(sensor);
         curr_angle += sensor->angle;
        // lcd_printf(degrees+curr_angle);
    }

    oi_setWheels(0, 0);     // Once desired degrees is reached the wheels stop
    timer_waitMillis(300);
}
/**
 *  this function is how we turn the robot
 *	@author Darron Anderson
 *	@param sensor_data the data from the robot
 *	@param degrees the number of degrees to turn from -179 to 180
 *	@date 12/7/18
 */
void turn_angle(int angle, oi_t *sensor_data ){
    int deg = angle;

    if (deg>0){
        turn_clockwise(sensor_data,angle);
    }
    if (deg<0){
        turn_counterclockwise(sensor_data,-angle);
    }
}
/**
 *  this function is how we move forward without checks
 *  if this were c++ it would be private
 *	@author Darron Anderson
 *	@param power the power to move with
 *	@date 12/7/18
 */
void move_forward(int power)
{
    oi_setWheels(power, power); //right
}
/**
*	this function is how we stop the robot
*	@author Darron Anderson
*	@date 12 / 7 / 18
*/
void stop()
{
 oi_setWheels(0,0);
}
/**
 *  this function is how we move back
 *  if this were c++ it would be private
 *  and should be used sparingly as no checks
 *	@author Darron Anderson
 *	@param sensor_data the data from the robot
 *	@param dist the distance to move back
 *	@param power the power to move with
 *	@date 12/7/18
 */
void move_back(oi_t* sensor_data,int dist,int power)
{
    int sum = 0;
    dist=-dist;
    oi_setWheels(-power,-power);
    while (sum> dist*10){
        oi_update(sensor_data);
        sum += sensor_data->distance;
    }
}
/**
 *	this function is how move forward safely in autonomous mode
 *	@author Darron Anderson
 *	@param sensor_data the data from the robot
 *	@param tar distance to move forward
 *	@date 12/7/18
 */
void safe_move_forward(oi_t* sensor_data,int tar)
    {

        int dist = 0;
        oi_setWheels(100,100);

        while(dist < tar)
        {
            oi_setWheels(100,100);
            char status[500];


            oi_update(sensor_data);
            dist += sensor_data->distance;
            int i;
            int start;
            if((i=drop_check(sensor_data)))  // If drop check returns a value something is wrong
            {
                stop();
                start=sensor_data->distance;
                if(i==3) // Both wheels are off...
                {
                    lcd_printf("well shit");
                }
                if(i==1) // Left wheel is off back up and go right
                {

                    uart_sendStr("****************Wheel Drop Left*****************");
                    sprintf(status, "\n\r\n\rBump sensor_data( Left: %d   Right: %d)\n\rCliff sensor_datas(Left: %d   Front left: %d   Front right: %d   Right: %d)\n\rCliff sensor_data Signals(Left: %d   Left Front: %d   Right Front: %d   Right: %d\n\r", sensor_data->bumpLeft, sensor_data->bumpRight, sensor_data->cliffLeft, sensor_data->cliffFrontLeft, sensor_data->cliffFrontRight, sensor_data->cliffRight, sensor_data->cliffLeftSignal, sensor_data->cliffFrontLeftSignal, sensor_data->cliffFrontRightSignal, sensor_data->cliffRightSignal);
                    uart_sendStr(status);

                    drop_reaction(sensor_data, 1);
                }
                if(i==2) // Right wheel is off back up and go left
                {

                    uart_sendStr("****************Wheel Drop Right*****************");
                    sprintf(status, "\n\r\n\rBump sensor_data( Left: %d   Right: %d)\n\rCliff sensor_datas(Left: %d   Front left: %d   Front right: %d   Right: %d)\n\rCliff sensor_data Signals(Left: %d   Left Front: %d   Right Front: %d   Right: %d\n\r", sensor_data->bumpLeft, sensor_data->bumpRight, sensor_data->cliffLeft, sensor_data->cliffFrontLeft, sensor_data->cliffFrontRight, sensor_data->cliffRight, sensor_data->cliffLeftSignal, sensor_data->cliffFrontLeftSignal, sensor_data->cliffFrontRightSignal, sensor_data->cliffRightSignal);
                    uart_sendStr(status);

                    drop_reaction(sensor_data, 2);

                }
            }


            if((i=cliff_check(sensor_data))) // Cliff sensor has gone off
            {

                stop();
                start=sensor_data->distance;
                if ((i & 1)) { // If check returns 0001 Left sensor is flagged

                    uart_sendStr("\n****************Cliff Sensor Left*****************\n\r");
                    sprintf(status, "\n\r\n\rBump sensor_data( Left: %d   Right: %d)\n\rCliff sensor_datas(Left: %d   Front left: %d   Front right: %d   Right: %d)\n\rCliff sensor_data Signals(Left: %d   Left Front: %d   Right Front: %d   Right: %d\n\r", sensor_data->bumpLeft, sensor_data->bumpRight, sensor_data->cliffLeft, sensor_data->cliffFrontLeft, sensor_data->cliffFrontRight, sensor_data->cliffRight, sensor_data->cliffLeftSignal, sensor_data->cliffFrontLeftSignal, sensor_data->cliffFrontRightSignal, sensor_data->cliffRightSignal);
                    uart_sendStr(status);

                    cliff_reaction(sensor_data, 1);

                }
                if ((i & 2)) { // If check returns 0010 LeftCenter sensor is flagged

                    uart_sendStr("\n****************Cliff Sensor Left Center*****************\n\r");
                    sprintf(status, "\n\r\n\rBump sensor_data( Left: %d   Right: %d)\n\rCliff sensor_datas(Left: %d   Front left: %d   Front right: %d   Right: %d)\n\rCliff sensor_data Signals(Left: %d   Left Front: %d   Right Front: %d   Right: %d\n\r", sensor_data->bumpLeft, sensor_data->bumpRight, sensor_data->cliffLeft, sensor_data->cliffFrontLeft, sensor_data->cliffFrontRight, sensor_data->cliffRight, sensor_data->cliffLeftSignal, sensor_data->cliffFrontLeftSignal, sensor_data->cliffFrontRightSignal, sensor_data->cliffRightSignal);
                    uart_sendStr(status);

                    cliff_reaction(sensor_data, 2);

               }
               if ((i & 4)) { // If check returns 0100 RightCenter sensor is flagged

                   uart_sendStr("\n****************Cliff Sensor Right Center*****************\n\r");
                   sprintf(status, "\n\rleft cliff: white tape!\n\rBump sensor_data( Left: %d   Right: %d)\n\rCliff sensor_datas(Left: %d   Front left: %d   Front right: %d   Right: %d)\n\rCliff sensor_data Signals(Left: %d   Left Front: %d   Right Front: %d   Right: %d\n\r", sensor_data->bumpLeft, sensor_data->bumpRight, sensor_data->cliffLeft, sensor_data->cliffFrontLeft, sensor_data->cliffFrontRight, sensor_data->cliffRight, sensor_data->cliffLeftSignal, sensor_data->cliffFrontLeftSignal, sensor_data->cliffFrontRightSignal, sensor_data->cliffRightSignal);
                   uart_sendStr(status);

                   cliff_reaction(sensor_data, 3);

               }
               if ((i & 8)) { // If check returns 1000 Right sensor is flagged

                   uart_sendStr("****************Cliff Sensor Right*****************");
                   sprintf(status, "\n\right cliff: white tape!\n\rBump sensor_data( Left: %d   Right: %d)\n\rCliff sensor_datas(Left: %d   Front left: %d   Front right: %d   Right: %d)\n\rCliff sensor_data Signals(Left: %d   Left Front: %d   Right Front: %d   Right: %d\n\r", sensor_data->bumpLeft, sensor_data->bumpRight, sensor_data->cliffLeft, sensor_data->cliffFrontLeft, sensor_data->cliffFrontRight, sensor_data->cliffRight, sensor_data->cliffLeftSignal, sensor_data->cliffFrontLeftSignal, sensor_data->cliffFrontRightSignal, sensor_data->cliffRightSignal);
                   uart_sendStr(status);

                   cliff_reaction(sensor_data, 4);

               }
            }

            if((i=edge_check(sensor_data))) //  White line detected
            {
                stop();
                start=sensor_data->distance;
                if ((i & 1)) { // If check returns 0001 Left sensor is flagged

                    uart_sendStr("\n****************Edge Sensor Left*****************\n\r");
                    sprintf(status, "\n\r\n\rBump sensor_data( Left: %d   Right: %d)\n\rCliff sensor_datas(Left: %d   Front left: %d   Front right: %d   Right: %d)\n\rCliff sensor_data Signals(Left: %d   Left Front: %d   Right Front: %d   Right: %d\n\r", sensor_data->bumpLeft, sensor_data->bumpRight, sensor_data->cliffLeft, sensor_data->cliffFrontLeft, sensor_data->cliffFrontRight, sensor_data->cliffRight, sensor_data->cliffLeftSignal, sensor_data->cliffFrontLeftSignal, sensor_data->cliffFrontRightSignal, sensor_data->cliffRightSignal);
                    uart_sendStr(status);

                    edge_reaction(sensor_data, 1);

                }
                if ((i & 2)) { // If check returns 0010 LeftCenter sensor is flagged

                    uart_sendStr("\n****************Edge Sensor Left Center*****************\n\r");
                    sprintf(status, "\n\r\n\rBump sensor_data( Left: %d   Right: %d)\n\rCliff sensor_datas(Left: %d   Front left: %d   Front right: %d   Right: %d)\n\rCliff sensor_data Signals(Left: %d   Left Front: %d   Right Front: %d   Right: %d\n\r", sensor_data->bumpLeft, sensor_data->bumpRight, sensor_data->cliffLeft, sensor_data->cliffFrontLeft, sensor_data->cliffFrontRight, sensor_data->cliffRight, sensor_data->cliffLeftSignal, sensor_data->cliffFrontLeftSignal, sensor_data->cliffFrontRightSignal, sensor_data->cliffRightSignal);
                    uart_sendStr(status);

                    edge_reaction(sensor_data, 2);

                }
                if ((i & 4)) { // If check returns 0100 RightCenter sensor is flagged

                    uart_sendStr("\n****************Edge Sensor Right Center*****************\n\r");
                    sprintf(status, "\n\rleft cliff: white tape!\n\rBump sensor_data( Left: %d   Right: %d)\n\rCliff sensor_datas(Left: %d   Front left: %d   Front right: %d   Right: %d)\n\rCliff sensor_data Signals(Left: %d   Left Front: %d   Right Front: %d   Right: %d\n\r", sensor_data->bumpLeft, sensor_data->bumpRight, sensor_data->cliffLeft, sensor_data->cliffFrontLeft, sensor_data->cliffFrontRight, sensor_data->cliffRight, sensor_data->cliffLeftSignal, sensor_data->cliffFrontLeftSignal, sensor_data->cliffFrontRightSignal, sensor_data->cliffRightSignal);
                    uart_sendStr(status);

                    edge_reaction(sensor_data, 3);

                }
                if ((i & 8)) { // If check returns 1000 Right sensor is flagged

                    uart_sendStr("****************Edge Sensor Right*****************");
                    sprintf(status, "\n\right cliff: white tape!\n\rBump sensor_data( Left: %d   Right: %d)\n\rCliff sensor_datas(Left: %d   Front left: %d   Front right: %d   Right: %d)\n\rCliff sensor_data Signals(Left: %d   Left Front: %d   Right Front: %d   Right: %d\n\r", sensor_data->bumpLeft, sensor_data->bumpRight, sensor_data->cliffLeft, sensor_data->cliffFrontLeft, sensor_data->cliffFrontRight, sensor_data->cliffRight, sensor_data->cliffLeftSignal, sensor_data->cliffFrontLeftSignal, sensor_data->cliffFrontRightSignal, sensor_data->cliffRightSignal);
                    uart_sendStr(status);

                    edge_reaction(sensor_data, 1);
                }

            }



            if(i=bump_check(sensor_data))
            {
                if (i==1) { // Bump Left

                    uart_sendStr("****************Bump Left*****************");
                    sprintf(status, "\n\r\n\rBump sensor_data( Left: %d   Right: %d)\n\rCliff sensor_datas(Left: %d   Front left: %d   Front right: %d   Right: %d)\n\rCliff sensor_data Signals(Left: %d   Left Front: %d   Right Front: %d   Right: %d\n\r", sensor_data->bumpLeft, sensor_data->bumpRight, sensor_data->cliffLeft, sensor_data->cliffFrontLeft, sensor_data->cliffFrontRight, sensor_data->cliffRight, sensor_data->cliffLeftSignal, sensor_data->cliffFrontLeftSignal, sensor_data->cliffFrontRightSignal, sensor_data->cliffRightSignal);
                    uart_sendStr(status);

                    bumper_reaction(sensor_data, 2);

                    dist += 300;


                }
                if (i==2) { // Bump Right

                    uart_sendStr("****************Bump Right*****************");
                    sprintf(status, "\n\r\n\rBump sensor_data( Left: %d   Right: %d)\n\rCliff sensor_datas(Left: %d   Front left: %d   Front right: %d   Right: %d)\n\rCliff sensor_data Signals(Left: %d   Left Front: %d   Right Front: %d   Right: %d\n\r", sensor_data->bumpLeft, sensor_data->bumpRight, sensor_data->cliffLeft, sensor_data->cliffFrontLeft, sensor_data->cliffFrontRight, sensor_data->cliffRight, sensor_data->cliffLeftSignal, sensor_data->cliffFrontLeftSignal, sensor_data->cliffFrontRightSignal, sensor_data->cliffRightSignal);
                    uart_sendStr(status);

                    bumper_reaction(sensor_data, 1);

                    dist += 300;

                }
            }

    }
       // if()
   stop();
}
/**
 *	this function is how we react to bumps in autonomous mode
 *	@author Darron Anderson
 *	@param sensor_data the data from the robot
 *	@param direction which wheel dropped
 *	@date 12/7/18
 */
void bumper_reaction(oi_t* sensor_data, int direction) {
    int i = 0;
    int j = 0;

    if (direction == 1) { // Right Bumper was hit

        i = -90;
        j = 90;

    } else if (direction == 2) { // Left Bumper was hit

        i = 90;
        j = -90;

    }

    move_back(sensor_data,10,100);

    turn_angle(i, sensor_data); // Turn 90, Move 10 centimeters
    safe_move_forward(sensor_data,100);

    turn_angle(j, sensor_data); // Turn 90, Move 30 centimeters past object
    safe_move_forward(sensor_data, 300);
    turn_angle(j, sensor_data); // Turn 90, Move 10 centimeters back to line
    safe_move_forward(sensor_data, 100);

    turn_angle(i, sensor_data); // Turn 90 back to original direction

}
/**
 *	this function is how we react to cliffs in autonomous mode
 *	@author Darron Anderson
 *	@param sensor_data the data from the robot
 *	@param res the result of cliff check
 *	@date 12/7/18
 */
void cliff_reaction(oi_t* sensor_data, int res) {

    if (res == 1) { // Left Sensor

        move_back(sensor_data, 5, 100); // Move Back 5 centimeters

        turn_angle(90, sensor_data); // Turn Right 90, Move 10 centimeters
        safe_move_forward(sensor_data, 100);

        turn_angle(-90, sensor_data); // Turn Left 90 back to original direction

    } else if (res == 2) { // Left Center Sensor

        move_back(sensor_data, 5, 100); // Move Back 5 centimeters

        turn_angle(60, sensor_data); // Turn Right 60, Move 10 centimeters
        safe_move_forward(sensor_data, 100);

        turn_angle(-60, sensor_data); // Turn Left 60 back to original direction

    } else if (res == 3) { // Right Center Sensor

        move_back(sensor_data, 5, 100); // Move Back 5 centimeters

        turn_angle(-60, sensor_data); // Turn Left 90, Move 10 centimeters
        safe_move_forward(sensor_data, 100);

        turn_angle(60, sensor_data); // Turn Right 90 back to original direction

    } else if (res == 4) { // Right Sensor

        move_back(sensor_data, 5, 100); // Move Back 5 centimeters

        turn_angle(-90, sensor_data); // Turn Left 90, Move 10 centimeters
        safe_move_forward(sensor_data, 100);

        turn_angle(90, sensor_data); // Turn Right 90 back to original direction

    }
}
/**
 *	this function is how we react to edges in autonomous mode
 *	@author Darron Anderson
 *	@param sensor_data the data from the robot
 *	@param res which sensor is detecting an edge
 *	@date 12/7/18
 */
void edge_reaction(oi_t *sensor_data, int res) {

    if (res == 1) { // Left Sensor, Correct orientation move to center of tile

         turn_angle(90, sensor_data);

         safe_move_forward(sensor_data, 100);

         turn_angle(-90, sensor_data);

    } else if (res == 2) { // Left Center Sensor

        while(!(edge_check(sensor_data)==1)) { // Get to correct orientation

            turn_angle(5, sensor_data);
        }

        turn_angle(90, sensor_data); // Move to center of tile

        safe_move_forward(sensor_data, 100);

        turn_angle(-90, sensor_data);

    } else if (res == 3) { // Right Center Sensor

        while(!(edge_check(sensor_data)==4)) { // Get to correct orientation
            turn_angle(-5, sensor_data);
        }

        turn_angle(-90, sensor_data); // Move to center of tile

        safe_move_forward(sensor_data, 100);

        turn_angle(90, sensor_data);

    } else if (res == 4) { // Right Sensor, Correct orientation move to center of tile

        turn_angle(-90, sensor_data);

        safe_move_forward(sensor_data, 100);

        turn_angle(90, sensor_data);

    }

}
/**
 *	this function is how we react to drops in autonomous mode
 *	@author Darron Anderson
 *	@param sensor_data the data from the robot
 *	@param direction which wheel dropped
 *	@date 12/7/18
 */
void drop_reaction(oi_t *sensor_data, int direction) {

    int i = 0;
    int j = 0;

    if (direction == 1) { // Left Wheel Drop

        i = -90;
        j = 90;

    } else if (direction == 2) { // Right Wheel Drop

        i = 90;
        j = -90;
    }

    move_back(sensor_data,10,500);

    turn_angle(j, sensor_data);

    safe_move_forward(sensor_data,10);

    turn_angle(i, sensor_data);
}

